# Flash cards app

## O aplikaci
Aplikace slouží k učení se pomocí flash cards. Každá karta má stranu se slovíčkem a překladem (nebo pojmem a vysvětlením) a kliknutím se karta otočí a ukáže rub karty. Pořadí ukazovaných karet z balíčku je náhodné.

V aplikaci můžeme zakládat nové balíčky na stránce create deck, která je přístupná z domovské stránky nebo bočního/vrchního menu (v závislosti na obrazovce). Do balíčku lze přidat až 50 karet. Strana karty je limitovaná pouze na 40 znaků, protože aplikace slouží k učení slovíček nebo krátkých pojmů.

Na balíčky, které jsme vytvořili, se můžeme podívat na stránce Browse decks. Odtud můžeme balíček spustit, upravit nebo vymazat.

Úprava balíčku je podobná jeho tvoření, ale přidává možnost prohodit obsah přední a zadní strany karty. Díky tomu můžeme balíček použít jak pro učení překladu z ČJ do ANJ tak i z ANJ do ČJ.

Aplikace ukládá balíčky pomocí místního úložiště v prohlížeči. Balíčky, které si vytvoříme budou tedy pouze v používaném prohlížeči na našem zařízení.

## Jak webovou stránku spustit
Ke spuštění stačí webový server, jako je Apache2 nebo Nginx. Pro tvorbu webové stránky nebyl využítý žádný z JavaScriptových frameworků a k nasazení tedy stačí na webový server přidat složku s webovou stránkou. Stránka je zveřejněná na GitLab pages v tomto repozitáři pomocí programu Jekyll. Jak se webový server tvoří najdete v konfiguraci CI/CD .gitlab-ci.yml. Jekyll pouze překopíruje soubory webové stránky do složky ./public, která slouží jako kořen pro GitLab pages.

## O implemetaci
Snažil jsem se do práce zakomponovat co nejvíce bodů z hodnotící tabulky semestrální práce, ale některé body nevyhovovali druhu aplikace.

- Offline aplikace (HTML a JS) - aplikace neposílá volání na externí API jiných služeb. Nedávalo tedy smysl použít service worker pro cachování výsledků HTTP volání.

- Vendor prefixes - během psaní aplikace jsem nepoužil cutting-edge css vlastnosti, u kterých by bylo potřeba dodat vendor prefix.

- Použití frameworku/ knihovny - aplikaci jsem se rozhodl psát v normálním JS a neměl jsem potřebu použít knihovnu. (tohle považuji za splnitelné vzhledem k zadání, ale v práci to není)

- JS práce s SVG - v práci nebylo potřeba upravovat SVG pomocí JS, vzhledem k tomu, že je obsažené především v tlačítkách.

Kde jsou v kódu splněné vybrané body:

- sémantické značky - nav, header, footer

- SVG v HTML - ikonky pro listování v balíčku, ikonky v prohlížeči balíčku.

- Média (HTML a JS) - listování balíčkem přehraje zvuk při změně karty.

- Pokročilé selektory - flash-cards.css je používá pro realizaci otočení karty, pseudo třídy jsou hlavně u tlačítek.

- Transformace - animace otočení karty

- Transitiony a animace - fade-in pro formulář, podtržení tlačítek v nav menu, listování karet

- Media queries - změna druhu menu, upravení fore a prohlížení balíčků

- OOP - třídy pro formulář na vytváření a editování balíčku

- Pokročilé JS API - LocalStorage na uložení balíčku

- Historie - v souboru navigation.js

## Pro testování fontů a důvod, proč se v kartách mění velikost písma
Japonské kanji mohou často být velmi složité a proto zvětšuji krátký text, aby bylo písmo dobře čitelné. Např. dny v japonském kanji (pondělí-neděle):　月曜日，火曜日，水曜日，木曜日，金曜日，土曜日，日曜日. Nebo já = 私，jíst = 食べる.
