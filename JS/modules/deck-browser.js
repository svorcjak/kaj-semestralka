import {handleOtherClicks} from "../utils/navigation.js";

// class for displaying saved decks
export function makeDeckBrowser() {
    const main = document.querySelector("main")
    main.innerHTML = ""
    const deckNames = Object.keys(localStorage)
    generateHtml(deckNames)
    addEventListeners()
}

// generate html for deck browser
function generateHtml(deckNames){
    const main = document.querySelector("main")
    main.innerHTML = ""
    main.innerHTML = "<h2>Decks</h2>\n" +
        "            <ul class=\"deck-browser-list\">\n" +
        "            </ul>"

    const deckBrowserList = document.querySelector(".deck-browser-list")

    deckNames.forEach(deckName => {
        if (validateDeck(deckName)){
            const deck = document.createElement("li")
            deck.innerHTML = "                    <span class=\"deck-name\">" + deckName + "</span>\n" +
                "                    <div class=\"browser-button-wrapper\">\n" +
                "                        <button class=\"play-button\"><img src=\"icons/play.svg\">Play</button>\n" +
                "                        <button class=\"edit-button\"><img src=\"icons/edit.svg\"> Edit</button>\n" +
                "                        <button class=\"delete-button\"><img src=\"icons/trashcan-white.svg\">Delete</button>\n" +
                "                    </div>"
            deck.classList.add("deck-browser-item")
            deckBrowserList.appendChild(deck)
        }
    })
}

// add event listeners to deck browser to play, edit, and delete decks
function addEventListeners(){
    const deckBrowserList = document.querySelector(".deck-browser-list")
    const deckBrowserItems = deckBrowserList.querySelectorAll(".deck-browser-item")

    deckBrowserItems.forEach(deckBrowserItem => {
        const deckName = deckBrowserItem.querySelector(".deck-name").innerText
        const playButton = deckBrowserItem.querySelector(".play-button")
        const editButton = deckBrowserItem.querySelector(".edit-button")
        const deleteButton = deckBrowserItem.querySelector(".delete-button")

        playButton.addEventListener('click', () => {
            handleOtherClicks("card-browser", encodeURIComponent(deckName))
        })

        editButton.addEventListener('click', () => {
            handleOtherClicks("deck-edit", encodeURIComponent(deckName))
        })

        deleteButton.addEventListener('click', () => {
            localStorage.removeItem(deckName)
            deckBrowserItem.remove()
        })
    })
}


// checks if the local storage item is a valid JSON object of a deck
function validateDeck(deckName) {
    const deck = JSON.parse(localStorage.getItem(deckName))
    const validKeys = ["name", "cards"]
    const validCardKeys = ["front", "back"]

    if(!Object.keys(deck).every((value, index) => value === validKeys[index])) {
        return false
    }

    if(deck.name !== deckName) {
        return false
    }

    deck.cards.forEach(card => {
        if(!Object.keys(card).every((value, index) => value === validCardKeys[index])) {
            return false
        }
    })

    return true
}