// class used for playing a deck of cards

export default class CardController {
    constructor(deck) {
        this.generateCardHtml();
        this.flashcards = Array.from(document.querySelectorAll('.flash-card'));
        this.counter = document.querySelector(".card-number");
        this.addEventListeners();

        this.currentDeck = JSON.parse(localStorage.getItem(deck));
        this.currentIndex = 0;
        this.cardIndex = 0;

        this.currentDeck.cards.sort(() => Math.random() - 0.5);

        this.renderCard();
    }

    // generates the html for the card and adds it to the DOM
    generateCardHtml() {
        const main = document.querySelector('main');
        main.innerHTML = "<!-- flash card -->\n" +
            "    <div class=\"flash-cards-wrapper\">\n" +
            "       <div class=\"flash-cards\">\n" +
            "           <div class=\"flash-card\" id=\"flash-card-0\">\n" +
            "               <div class=\"flash-card-inner\">\n" +
            "                   <div class=\"flash-card-face flash-card-front\">\n" +
            "                       <p></p>\n" +
            "                   </div>\n" +
            "                   <div class=\"flash-card-face flash-card-back\">\n" +
            "                       <p></p>\n" +
            "                   </div>\n" +
            "               </div>\n" +
            "           </div>\n" +
            "           <div class=\"flash-card\" id=\"flash-card-1\">\n" +
            "               <div class=\"flash-card-inner\">\n" +
            "                   <div class=\"flash-card-face flash-card-front\">\n" +
            "                       <p></p>\n" +
            "                   </div>\n" +
            "                   <div class=\"flash-card-face flash-card-back\">\n" +
            "                       <p></p>\n" +
            "                   </div>\n" +
            "               </div>\n" +
            "           </div>\n" +
            "       </div>\n" +
            "       <!-- flash card navigation buttons -->\n" +
            "       <div class=\"buttons\">\n" +
            "           <button class=\"btn-card\" id=\"back-btn\"><img src='./icons/arrow-left.svg'></button>\n" +
            "           <p class=\"card-number\"></p>\n" +
            "           <button class=\"btn-card\" id=\"forward-btn\"><img src='./icons/arrow-right.svg'></button>\n" +
            "           <audio id='click-sound' src='sounds/click.wav'></audio>\n" +
            "       </div>\n" +
            "       <span>Click on the card to flip it</span>\n" +
            "   </div>";
    }

    // Adds event listeners to the flash cards and the navigation buttons
    addEventListeners() {
        const forwardButton = document.querySelector("#forward-btn");
        const backButton = document.querySelector("#back-btn");
        const clickSound = document.querySelector("#click-sound");

        for (const flashcard of this.flashcards) {
            flashcard.addEventListener('click', () => {
                flashcard.classList.toggle('flash-card-flip');
            });
        }

        forwardButton.addEventListener('click', () => {
            this.nextCard();
            this.playSound(clickSound);
        });

        backButton.addEventListener('click', () => {
            this.prevCard();
            this.playSound(clickSound);
        });
    }

    // switches to the next card in the deck and updates the card counter, it only uses 2 cards and switches between them for the entire deck
    nextCard() {
        if (this.cardIndex < this.currentDeck.cards.length - 1) {
            this.cardIndex++;
        } else {
            this.cardIndex = 0;
        }
        this.currentIndex = (this.currentIndex + 1) % 2;
        this.renderCard();
    }

    // switches to the previous card in the deck and updates the card counter, it only uses 2 cards and switches between them for the entire deck
    prevCard() {
        if (this.cardIndex > 0) {
            this.cardIndex--;
        } else {
            this.cardIndex = this.currentDeck.cards.length - 1;
        }
        this.currentIndex = (this.currentIndex + 1) % 2;
        this.renderCard();
    }

    // switches between the two cards and updates the text content of the new card
    renderCard() {
        const card = this.currentDeck.cards[this.cardIndex];
        const front = document.querySelector(`#flash-card-${this.currentIndex} .flash-card-front p`);
        const back = document.querySelector(`#flash-card-${this.currentIndex} .flash-card-back p`);
        front.textContent = card.front;
        back.textContent = card.back;

        this.changeFontSize(".flash-card-face p"); // changes font size of shorter text for better readability

        this.counter.textContent = (this.cardIndex + 1) + "/" + this.currentDeck.cards.length; // updates card counter in DOM

        // set the inactive card to active and vice versa, also unflips the hidden card if it was flipped
        this.flashcards.forEach((card, i) => {
            if (i === this.currentIndex) {
                card.classList.add('active');
            } else {
                card.classList.remove('active');
                card.classList.remove('flash-card-flip');
            }
        });
    }

    // changes the font size of the text in the card depending on the length of the text
    changeFontSize(selector) {
        for (const element of document.querySelectorAll(selector)) {
            const textlength = element.textContent.length;
            element.style.fontSize = "calc(16px + " + Math.max(7-textlength, 1) + "rem)";
            element.style.fontWeight = "lighter";
        }
    }

    // plays an audio element from DOM, it replays the sound if it is already playing
    playSound(sound) {
        if (!sound.paused){
            sound.pause();
            sound.currentTime = 0;
        }
        sound.play();
    }
}