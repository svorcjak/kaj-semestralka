import { handleOtherClicks } from "../utils/navigation.js";

export function generateHome() {
    const main = document.querySelector("main")
    main.innerHTML = ""
    main.innerHTML = "<!-- home -->\n" +
        "    <div class=\"home-wrapper\">\n" +
        "        <div class=\"home\">\n" +
        "            <h1>Flash Cards</h1>\n" +
        "            <p>Flash Cards is a simple app that lets you create and study flash cards. It was created primarily for studying foreign languages. Specifically japanese.</p>\n" +
        "            <p>If you are new, click on the 'Create Deck' button to get started. </p>\n" +
        "            <button class=\"btn\" id=\"create-deck\">Create Deck</button>\n" +
        "        </div>\n" +
        "    </div>";

    document.querySelector("#create-deck").addEventListener("click", () => {
        handleOtherClicks("deck-creator")
    })
}