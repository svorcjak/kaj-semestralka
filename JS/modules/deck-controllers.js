import { handleOtherClicks, replaceState } from "../utils/navigation.js";

// base class for creating or editing a deck
class DeckController {
    constructor(deck) {
        this.deck = JSON.parse(localStorage.getItem(deck))
        this.currentNumberOfCards = 0

        this.generateHtml()
        this.addEventListeners()
    }

    // generates the html form for the deck creation form and adds it to the DOM
    generateHtml() {
        const main = document.querySelector('main')
        main.innerHTML = "<!-- Deck creation form -->\n" +
            "            <form class=\"deck-creation-form\">\n" +
            "                <h2></h2>\n" +
            "                <div>\n" +
            "                    <label for=\"deck-name\">Deck name</label>\n" +
            "                    <input id=\"deck-name\" type=\"text\" name=\"deck-name\" placeholder=\"My new deck\" maxlength='20'>\n" +
            "                </div>\n" +
            "                <div class=\"cards\">\n" +
            "                    <ul>\n" +
            "                    </ul>\n" +
            "                    <button id=\"add-card\" type=\"button\">Add card</button>\n" +
            "                </div>\n" +
            "                <div class=\"error-messages\">\n" +
            "                </div>\n" +
            "                <button id=\"deck-submit\" type=\"button\"></button>\n" +
            "            </form>";
    }

    // Adds event listeners to the form for adding a card to the deck
    addEventListeners() {
        const addCard = document.querySelector("#add-card")
        addCard.addEventListener('click', () => {
            this.addCard()
        })
    }

    // reads the form and creates a deck json object from it
    readDeckForm() {
        // init
        const deckName = document.querySelector("#deck-name").value.trim();
        const errors = new Set();
        let e = this.validateDeckName(deckName); e !== undefined ? errors.add(e) : null;

        // select all card elements
        const cards = document.querySelectorAll(".card")
        let cardArray = []

        // loop through all cards and read their values
        for (const card of cards) {
            const front = card.querySelector(".card-front").value.trim();
            const back = card.querySelector(".card-back").value.trim();

            const sides = [front, back];

            let [err, markedFields] = this.validateCard(sides); // validate card
            err.forEach(e => errors.add(e)); // add errors to set

            this.repaintInputFields(markedFields, card); // changes the border color of the input fields to red if they are invalid

            cardArray.push({front: front, back: back}); // push card to array
        }

        // check for empty deck
        if (cardArray.length === 0) {
            errors.add("Deck must contain at least one card");
        }

        // display errors if present
        if(errors.size > 0) {
            this.appendErrorMessages(errors);
            return {};
        }

        return {name: deckName, cards: cardArray};
    }

    // validates inputs for a card
    validateCard(sides) {
        const errors = [];
        const markedFields = {front: false, back: false}

        // loop through sides and validate them
        sides.forEach((side, i) => {
            switch (true) {
                case side === "" || side === undefined:
                    errors.push("Card side cannot be empty");
                    markedFields[i === 0 ? "front" : "back"] = true;
                    break;
                case side.length > 40:
                    errors.push("Card side cannot be longer than 40 characters");
                    markedFields[i === 0 ? "front" : "back"] = true;
                    break;
            }
        })
        return [errors, markedFields];

    }

    // validates the deck name
    validateDeckName(deckName) {
        switch (true) {
            case deckName === "" || deckName === undefined:
                return "Deck name cannot be empty"
            case deckName.length > 20:
                return "Deck name cannot be longer than 20 characters"
        }
    }

    // changes the border color of the input fields to red if they are invalid
    repaintInputFields(markedFields, card) {
        if (markedFields.front) {
            card.querySelector(".card-front").style.borderColor = "red";
        }
        else {
            card.querySelector(".card-front").style.borderColor = "#ccc";
        }
        if (markedFields.back) {
            card.querySelector(".card-back").style.borderColor = "red";
        }
        else {
            card.querySelector(".card-back").style.borderColor = "#ccc";
        }
    }

    // appends a new card to the deck creation form
    insertCardHTML() {
        let cards = document.querySelector(".cards ul")
        let card = document.createElement("li")
        card.innerHTML = "<div class=\"card\">\n" +
            "                            <label>Front\n" +
            "                                <input class=\"card-front\" type=\"text\" name=\"card-front\" placeholder=\"Front side of the card\" maxlength=\"40\">\n" +
            "                            </label>\n" +
            "                            <label>Back\n" +
            "                                <input class=\"card-back\" type=\"text\" name=\"card-back\" placeholder=\"Back side of the card\" maxlength=\"40\">\n" +
            "                            </label>\n" +
            "                            <button class=\"remove-card\" type=\"button\"><img src=\"icons/trashcan.svg\" alt=''></button>\n" +
            "                        </div>"

        card.classList.add("fadeIn")

        const removeCard = card.querySelector(".remove-card")

        // adds event listener to the remove card button
        removeCard.addEventListener('click', () => {
            card.remove()
            this.currentNumberOfCards--
            document.querySelector("#add-card").disabled = false
        })

        cards.appendChild(card)
    }

    // inserts a new card into the deck creation form and if the form has 50 cards, disables the add card button
    addCard() {
        this.insertCardHTML()
        const main = document.querySelector('main')
        main.scrollTo(0, main.scrollHeight)
        this.currentNumberOfCards++

        if(this.currentNumberOfCards >= 50) {
            document.querySelector("#add-card").disabled = true
        }
    }

    // shows error messages on the form
    appendErrorMessages(errorMessages) {
        const deckName = document.querySelector("#deck-name")
        if(errorMessages.has("Deck name cannot be empty") || errorMessages.has("Deck name cannot be longer than 20 characters")) {
            deckName.style.borderColor = "red"
        }
        const target = document.querySelector(".error-messages")
        target.innerHTML = ""

        errorMessages.forEach(errorMessage => {
            const error = document.createElement("p")
            error.innerText = errorMessage
            error.classList.add("error")
            target.appendChild(error)
        })

        const main = document.querySelector('main')
        main.scrollTo(0, main.scrollHeight)
    }

    // shows error message if the deck name already exists
    duplicateDeckNameInvalid() {
        const deckName = document.querySelector("#deck-name")
        deckName.style.borderColor = "red"

        const target = document.querySelector(".error-messages")

        target.innerHTML = ""

        const error = document.createElement("p")
        error.innerText = "Deck name already exists"
        target.appendChild(error)
    }
}


// class for the deck creation page
export class DeckCreationController extends DeckController {
    constructor() {
        super({})
        this.addCard()
    }

    // generates the html for the deck creation page and changes the text of the submit button and the heading
    generateHtml() {
        super.generateHtml()
        document.querySelector(".deck-creation-form h2").innerText = "Create a new deck"
        document.querySelector("#deck-submit").innerText = "Create deck"
    }

    // reads the deck creation form and saves the deck to local storage if it is not a duplicate
    createDeck() {
        this.deck = this.readDeckForm()

        if (Object.keys(this.deck).length === 0) {
            return;
        }

        if (!localStorage.getItem(this.deck.name)) {
            localStorage.setItem(this.deck.name, JSON.stringify(this.deck))
            replaceState("deck-edit", this.deck.name)
            handleOtherClicks("deck-browser")
        }
        else {
            this.duplicateDeckNameInvalid()
        }
    }

    // adds an event listener to the submit button
    addEventListeners() {
        super.addEventListeners();

        const deckSubmit = document.querySelector("#deck-submit")
        deckSubmit.addEventListener('click', () => {
            this.createDeck()
        })
    }
}

// class for the deck edit page
export class DeckEditController extends DeckController {
    constructor(deck) {
        super(deck)
        this.originalName = this.deck.name;
    }

    // generates the form and adds the cards from the deck to it
    generateHtml() {
        super.generateHtml()

        document.querySelector("#deck-name").value = this.deck.name

        this.deck.cards.forEach(card => {
            super.addCard()
            let cardElement = document.querySelector(".cards ul").lastChild
            cardElement.querySelector(".card-front").value = card.front
            cardElement.querySelector(".card-back").value = card.back
        })

        document.querySelector(".deck-creation-form h2").innerText = "Edit deck"
        document.querySelector("#deck-submit").innerText = "Save changes"

        // create a button to swap the front sides and backsides of cards
        const swapButton = document.createElement("button")
        swapButton.innerText = "Swap sides"
        swapButton.id = "swap-sides"
        swapButton.type = "button"
        swapButton.addEventListener('click', () => {
            this.swapSides();
        })
        document.querySelector(".cards").appendChild(swapButton)
    }

    // swaps the front and back sides of all cards in the form
    swapSides() {
        const cards = document.querySelectorAll(".card")
        cards.forEach(card => {
            const front = card.querySelector(".card-front")
            const back = card.querySelector(".card-back")
            const temp = front.value
            front.value = back.value
            back.value = temp
        })
    }

    // adds an event listener to the submit button
    addEventListeners() {
        super.addEventListeners();

        const deckSubmit = document.querySelector("#deck-submit")
        deckSubmit.addEventListener('click', () => {
            this.updateDeck()
        })
    }

    // reads the deck edit form and saves the edited deck to local storage if it is not a duplicate of another deck
    updateDeck() {
        this.deck = this.readDeckForm()

        if (Object.keys(this.deck).length === 0) {
            return;
        }

        if (this.deck.name === this.originalName) {
            localStorage.setItem(this.deck.name, JSON.stringify(this.deck))
            handleOtherClicks("deck-browser")
        }
        else if (!localStorage.getItem(this.deck.name)) {
            localStorage.removeItem(this.originalName)
            localStorage.setItem(this.deck.name, JSON.stringify(this.deck))

            // change url for history api and redirect to deck browser
            replaceState("deck-edit", this.deck.name)
            handleOtherClicks("deck-browser")
        }
        else {
            this.duplicateDeckNameInvalid();
        }
    }
}