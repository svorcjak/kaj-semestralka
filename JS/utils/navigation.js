import {DeckCreationController, DeckEditController} from "../modules/deck-controllers.js";
import CardController from "../modules/card-controller.js";
import {makeDeckBrowser} from "../modules/deck-browser.js";
import { generateHome } from "../modules/home.js";

// used to generate content based on the url when the page loads and when the back/forward button is pressed
export function handleHistory() {
    const url = window.location.href;
    if (url.indexOf('?') === -1) {
        return;
    }
    generateContent(url);
}

// used to generate content based on the data-page attribute when a nav link is clicked
export function handleNavLinkClick(event) {
    event.preventDefault();
    const url = window.location.href.split('?')[0] + '?page=' + this.getAttribute('data-page');

    generateContent(url)

    history.pushState(null, null, url);
}

// used to generate content based on the data-page attribute when a button is clicked when creating a deck etc.
export function handleOtherClicks(page, deck) {
    let url;
    if (deck === undefined) {
        url = window.location.href.split('?')[0] + '?page=' + page;
    }
    else {
        url = window.location.href.split('?')[0] + '?page=' + page + '&deck=' + deck;
    }

    generateContent(url);

    history.pushState(null, null, url);
}

// used to generate content based on the url
function generateContent(url){
    const queryString = url.split('?')[1];
    const params = queryString.split('&');
    const target = params[0].split('=')[1];

    switch (target) {
        case 'deck-creator':
            new DeckCreationController();
            break;
        case 'deck-browser':
            makeDeckBrowser();
            break;
        case 'card-browser':
            new CardController(decodeURIComponent(params[1].split('=')[1]));
            break;
        case 'deck-edit':
            new DeckEditController(decodeURIComponent(params[1].split('=')[1]));
            break;
        case 'home':
            generateHome();
            break;
    }
}

// replaces the current state with a new one when the deck name is changed
export function replaceState(page, deckName){
    let url;
    if (deckName === undefined) {
        url = window.location.href.split('?')[0] + '?page=' + page;
    }
    else {
        url = window.location.href.split('?')[0] + '?page=' + page + '&deck=' + deckName;
    }

    history.replaceState(null, null, url);
}
