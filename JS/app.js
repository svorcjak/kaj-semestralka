import { handleHistory, handleNavLinkClick } from './utils/navigation.js';

/* add event listeners for navigation */
window.addEventListener('popstate', handleHistory);
window.addEventListener('load', handleHistory);

const navLinks = document.querySelectorAll('nav a');
navLinks.forEach(function(navLink) {
    navLink.addEventListener('click', handleNavLinkClick);
})

/* start app on home page */
const homeNavLink = document.querySelector('nav a[data-page="home"]');
homeNavLink.click();
